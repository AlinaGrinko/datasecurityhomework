
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.*;
import org.apache.commons.io.IOUtils;

public class Main {
	public static byte[] Read(String name) {
		byte[] bytes={};
		
		try (FileInputStream fin = new FileInputStream(name)) {
			InputStreamReader in = new InputStreamReader(fin, "UTF-8");		
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			IOUtils.copy(in, out);
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(out);
			bytes = out.toByteArray();
			
		} catch (IOException ex) {

			System.out.println(ex.getMessage());
		}
		return bytes;
	}
	
	public static int[] Count(byte[] input){
		int[] res=new int[8];
		try{
			for(int i=0; i<input.length; i++){
				if ((input[i]&0x01)!=0){
					res[0]++;
				}
				if ((input[i]&0x02)!=0){
					res[1]++;
				}
				if ((input[i]&0x04)!=0){
					res[2]++;
				}
				if ((input[i]&0x08)!=0){
					res[3]++;
				}
				if ((input[i]&0x10)!=0){
					res[4]++;
				}
				if ((input[i]&0x20)!=0){
					res[5]++;
				}
				if ((input[i]&0x30)!=0){
					res[6]++;
				}
				if ((input[i]&0x40)!=0){
					res[7]++;
				}
			}		
		} catch (Exception ex) {

			System.out.println(ex.getMessage());
		}
		return res;	
	}


	public static void main(String[] args) {
		// �����, ��� � ���� ��� ������ �������������� � UTF-8
		// �����, ���� ���������� ���������, �� ���� ������ ������� �� 
		// ����, ����� ������� �������� ������� (� ������ �� ����� ��������
		// ��������� �����, �������� ��� ������� �����������)
		System.out.println("������� �����");
		byte[] arr=Read("russian.txt");
		System.out.println("����������� �������� - " + arr.length);
		int[] count=Count(arr);
		double length=arr.length;
		System.out.println("����������� ������� �� ������� :");
		for (int i=0; i<8; i++){
			System.out.println("���� �"+i+"->"+(count[i]/length));
		}
		System.out.println("���������� �����");
		arr=Read("ukrainian.txt");
		System.out.println("����������� �������� - " + arr.length);
		count=Count(arr);
		length=arr.length;
		System.out.println("����������� ������� �� ������� :");
		for (int i=0; i<8; i++){
			System.out.println("���� �"+i+"->"+(count[i]/length));
		}
		System.out.println("���������� �����");
		arr=Read("english.txt");
		System.out.println("����������� �������� - " + arr.length);
		count=Count(arr);
		length=arr.length;
		System.out.println("����������� ������� �� ������� :");
		for (int i=0; i<8; i++){
			System.out.println("���� �"+i+"->"+(count[i]/length));
		}
	}
}
