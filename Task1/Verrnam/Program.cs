﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Verrnam
{
    class Program
    {
        static bool xor(bool x, bool y)
        {
            return ((!x || !y) && (x || y));
        }

        static byte xor(byte x1, byte y1)
        {
            bool x = x1 == 1 ? true : false;
            bool y = y1 == 1 ? true : false;
            return (byte)((((!x || !y) && (x || y)) == true) ? 1 : 0);
        }

        static byte[] Encript(byte[] mesSet, byte[] keySet)
        {
            byte[] encoded = new byte[keySet.Length];
            for (int i = 0; i < mesSet.Length; i++)
            {
                int j = i / keySet.Length;
                encoded[i] = xor(mesSet[i],keySet[j]);                
            }
            return encoded;
        }

        static byte[] Decript(byte[] mesSet, byte[] keySet)
        {
            byte[] decoded = new byte[keySet.Length];
            for (int i = 0; i < mesSet.Length; i++)
            {
                int j = i / keySet.Length;
                decoded[i] = xor(mesSet[i], keySet[j]);
            }
            return decoded;
        }

        static void Main(string[] args)
        {
            string message = "Hello, Vasya!";
            string key = "asc";
            
            byte[] mesSet = Encoding.Default.GetBytes(message);
            byte[] keySet = Encoding.Default.GetBytes(key);

            byte[] b = Encript(mesSet, keySet);
            byte[] c = Decript(b, keySet);
    
            Console.WriteLine(b);
            Console.WriteLine(c);

            string res = Encoding.Default.GetString(c);
            Console.WriteLine(res);
            Console.ReadKey();
        }
    }
}