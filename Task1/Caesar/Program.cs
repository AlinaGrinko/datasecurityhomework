﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caesar
{
    class Program
    {
        static string alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        static string[] doub = {"СТ","НО","ЕН","ТО","НА","ОВ","НИ","РА","ВО","КО","СТО","ЕНО","НОВ","ТОВ","ОВО","ОВА" };

        static string Encript(string message, int shift) {
         
            string result="";

            foreach (var e in message.ToLower()) {
                int pos = alphabet.IndexOf(e);
                if (!(pos>=0))
                {
                    result += e;
                }
                else
                {
                    int move = pos + shift;
                    while (move > alphabet.Length-1)
                    {
                        move -= alphabet.Length;
                    }
                    while (move < 0)
                    {
                        move += alphabet.Length;
                    }
                    result += alphabet[move];
                }
            }
            return result;
        }

        static string Decript(string message)
        {
            string result = "";
            double counter = 0;
            Dictionary<char, int> alph = new Dictionary<char, int>();

            foreach (char e in message)
            {
                if (!alphabet.Contains(e))
                {
                    continue;
                }
                if (!alph.ContainsKey(e))
                {
                    alph.Add(e, 1);
                }
                else
                {
                    alph[e]++;
                }
                counter++;
            }

            var sortedDict = from entry in alph orderby entry.Value descending select entry;

            char freq = sortedDict.ElementAt(0).Key;
            int posOfFound = message.IndexOf(freq);
            int var1 = message.IndexOf("о");
            int var2 = message.IndexOf("е");
            int var3 = message.IndexOf("a");

            string s1 = Encript(message, var1- posOfFound-1);
            string s2 = Encript(message, var2- posOfFound - 1);
            string s3 = Encript(message, var3 - posOfFound - 1);

            int count1 = 0;
            int count2 = 0;
            int count3 = 0;

            foreach (var a in doub) {
                var e= a.ToLower();
                if (s1.IndexOf(e) >= 0) {
                    count1++;
                }
                if (s2.IndexOf(e) >= 0)
                {
                    count2++;
                }
                if (s3.IndexOf(e) >= 0)
                {
                    count3++;
                }
            }

            if ((count1 > count2) && (count1 > count3))
                result = s1;
            else if ((count2 > count1) && (count2 > count3))
                result = s2;
            else if ((count3 > count1) && (count3 > count2))
                result = s3;
            return result;
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Зашифровуем лекционный пример - криптоанализ шифра цезаря со сдвигом в 16");
            string enc = Encript("криптоанализ шифра цезаря", 16);
            Console.WriteLine(enc);
            Console.WriteLine("Расшифровываем его же");
            Console.WriteLine(Decript(enc));
            Console.ReadKey();
        }
    }
}
