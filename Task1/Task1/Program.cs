﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {
        static double counter;
        static Dictionary<char, int> alphabet = new Dictionary<char, int>();
        static char[] letters = {'й','ц','у','к','е','н','г','ш','щ','з','х',
            'ъ','ф','ы','в','а','п','р','о', 'л', 'д', 'ж','э','я','ч','с','м',
            'и','т','ь','б','ю','ё'};

        static void Counter(string filename)
        {
            alphabet.Clear();
            counter = 0;
            StreamReader file = new StreamReader(filename, Encoding.GetEncoding(1251));
            string str = file.ReadToEnd().ToLower();

            foreach (char e in str)
            {
                if (!letters.Contains(e)) {
                    continue;
                }
                if (!alphabet.ContainsKey(e))
                {
                    alphabet.Add(e, 1);
                }
                else {
                    alphabet[e]++;
                }
                counter++;
            }
        }


        static void Main(string[] args)
        {
            Counter("../../fairytale.txt");
            var sortedDict = from entry in alphabet orderby entry.Value descending select entry;
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("В сказке");
            foreach (var e in sortedDict) {
                Console.Write(e.Key+"->" + (e.Value/counter).ToString().Substring(0,5)+" ");
            }

            Console.WriteLine();
            Counter("../../publicspeach.txt");
            Console.WriteLine("В выступлении публичного лица");
            foreach (var e in sortedDict)
            {
                Console.Write(e.Key + "->" + (e.Value / counter).ToString().Substring(0, 5) + " ");
            }

            Console.WriteLine();
            Counter("../../fiction.txt");
            Console.WriteLine("В художественной литературе");
            foreach (var e in sortedDict)
            {
                Console.Write(e.Key + "->" + (e.Value / counter).ToString().Substring(0, 5) + " ");
            }

            Counter("../../technical.txt");
            Console.WriteLine();
            Console.WriteLine("В технической литературе");
            foreach (var e in sortedDict)
            {
                Console.Write(e.Key + "->" + (e.Value / counter).ToString().Substring(0, 5) + " ");
            }

            Console.ReadKey();
        }
    }
}
